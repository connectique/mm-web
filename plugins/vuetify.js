import Vue from 'vue'
import Vuetify from 'vuetify'

Vue.use(Vuetify, {
  theme: {
    primary: '#388CC8',
    accent: '#F386A6',
    secondary: '#F58121',
    info: '#0D47A1',
    warning: '#FFE500',
    error: '#E21F25',
    success: '#41AD4A'
  }
})
