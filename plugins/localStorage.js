import createPersistedState from 'vuex-persistedstate'
import * as Cookies from 'js-cookie'

export default ({store, isHMR, isClient}) => {
  if (isHMR) return

  if (isClient) {
    window.onNuxtReady(nuxt => {
      createPersistedState({
        storage: {
          getItem: key => Cookies.get(key),
          setItem: (key, value) => Cookies.set(key, value, { expires: 3 }),
          removeItem: key => Cookies.remove(key)
        }
      })(store)
    })
  }
}
