export const state = () => ({
  loggedIn: false,
  memberType: '',
  currentPage: '',
  currentViewingMatchingProfileId: '',
  currentEditingMatchingProfileId: '',
  currentViewingRecordId: '',
  errorSnackbar: false,
  errorText: '',
  successSnackbar: false,
  successText: '',
  currentChatPartnerId: '',
  successMessage: '',
  snackbar: false,
  typeOfViewingRecord: '',
  user: {},
  matchingProfileDetailData: {}
})

export const mutations = {
  logIn (state) {
    state.loggedIn = true
  },
  logOut (state) {
    state.loggedIn = false
    state.memberType = null
  },
  setMemberType (state, type) {
    state.memberType = type
  },
  setCurrentPage (state, page) {
    state.currentPage = page
  },
  setCurrentViewingMatchingProfileId (state, id) {
    state.currentViewingMatchingProfileId = id
  },
  setCurrentEditingMatchingProfileId (state, id) {
    state.currentEditingMatchingProfileId = id
  },
  setCurrentViewingRecordId (state, id) {
    state.currentViewingRecordId = id
  },
  setErrorSnackbar (state, val) {
    state.errorSnackbar = val
  },
  setErrorText (state, text) {
    state.errorText = text
  },
  setSuccessSnackbar (state, val) {
    state.successSnackbar = val
  },
  setSuccessText (state, text) {
    state.successText = text
  },
  setCurrentChatPartnerId (state, id) {
    state.currentChatPartnerId = id
  },
  setSuccessMessage (state, message) {
    state.successMessage = message
  },
  setSnackbar (state, val) {
    state.snackbar = val
  },
  setTypeOfViewingRecord (state, val) {
    state.typeOfViewingRecord = val
  },
  setUser (state, val) {
    state.user = val
  },
  setMatchingProfileDetailData (state, val) {
    state.matchingProfileDetailData = val
  }
}
