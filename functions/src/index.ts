import * as functions from 'firebase-functions';
import * as path from 'path'
import * as gcs from '@google-cloud/storage'
import { spawn } from 'child-process-promise'
import * as fs from 'fs'
import * as os from 'os'
import * as PubSub from '@google-cloud/pubsub'
import * as cors from 'cors'
import * as express from 'express'
import * as admin from 'firebase-admin'
admin.initializeApp(functions.config().firebase)

exports.generateThumbnail = functions.storage.object().onChange(event => {
  const object = event.data
  const fileBucket = object.bucket
  const filePath = object.name
  const contentType = object.contentType
  const resourceState = object.resourceState
  const metageneration = object.metageneration

  if (!contentType.startsWith('image/')) {
    console.log('This is not an image.')
    return null
  }

  const fileName = path.basename(filePath)
  if (fileName.startsWith('thumb_')) {
    console.log(filePath + ' is already a Thumbnail.')
    return null
  }

  if (resourceState === 'not_exists') {
    console.log('This is a deletion event.')
    return null
  }

  if (resourceState === 'exists' && metageneration > 1) {
    console.log('This is a metadata change event.')
    return null
  }

  const bucket = gcs().bucket(fileBucket)
  const tempFilePath = path.join(os.tmpdir(), fileName)
  const metadata = { contentType }
  return bucket.file(filePath).download({
    destination: tempFilePath
  })
    .then(() => {
      console.log('Image downloaded locally to', tempFilePath)
      return spawn('convert', [tempFilePath, '-thumbnail', '200x200>', tempFilePath])
        .then(() => {
          console.log('Thumbnail created at ', tempFilePath)
          const thumbFileName = `thumb_${fileName}`
          const thumbFilePath = path.join(path.dirname(filePath), thumbFileName)
          return bucket.upload(tempFilePath, { destination: thumbFilePath, metadata })
            .then(() => fs.unlinkSync(tempFilePath))
        })
    })
})

exports.matchalgorithmrequest = functions.https.onRequest((req, res) => {
  cors()(req, res, async () => {
    const pubsub = new PubSub({ projectId: 'connectiqueco' })
    const data = req.body
    const jsonData = JSON.stringify(data.message)
    const messageBuffer = Buffer.from(jsonData)

    if (!data.topicName) {
      res.status(400).json({
        error: 'request is missing topic name'
      })
      return
    }

    const publisher = pubsub.topic(data.topicName).publisher()

    try {
      const results = await publisher.publish(messageBuffer)
      console.log(results)
      const messageId = results[0]
      console.log(`Message ${messageId} published.`)
      res.status(201).json({
        messageId
      })
    } catch (err) {
      console.error(err)
      res.status(500).json({
        err
      })
    }
  })
})

const app = express()
app.use(cors())
app.use(function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*')
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
  next()
})
app.get('/count-users', async (req, res) => {
  const usersListObject = await admin.auth().listUsers()
  res.json({
    count: usersListObject.users.length
  })
})
app.get('/count-user-types', async (req, res) => {
  const userInfoSnapshot = await admin.firestore().collection('users').get()
  let educatorCount = 0
  let schoolCount = 0
  userInfoSnapshot.forEach(doc => {
    const data = doc.data()
    if (data.memberType === 'school') {
      schoolCount += 1
    }
    if (data.memberType === 'educator') {
      educatorCount += 1
    }
  })
  res.json({
    educatorCount,
    schoolCount
  })
})
app.get('/users-no-member-type', async (req, res) => {
  const userInfoSnapshot = await admin.firestore().collection('users').get()
  const users = []
  userInfoSnapshot.forEach(doc => {
    const data = doc.data()
    if (!data.memberType) {
      users.push({
        id: doc.id,
        data
      })
    }
  })
  res.json({
    users
  })
})
app.get('/users-no-public-info', async (req, res) => {
  const userInfoSnapshot = await admin.firestore().collection('publicUserInfo').get()
  const users = []
  userInfoSnapshot.forEach(doc => {
    const data = doc.data()
    const missingFields = []
    if (!data.avatarUrl) {
      missingFields.push('avatarUrl')
    }
    if (!data.description) {
      missingFields.push('description')
    }
    if (!data.displayName) {
      missingFields.push('displayName')
    }
    if (missingFields.length > 0) {
      users.push({
        id: doc.id,
        missingFields
      })
    }
  })
  res.json({
    users
  })
})
app.get('/count-matches', async (req, res) => {
  const matchRecordSnapshot = await admin.firestore().collection('matchRecords').where('valid', '==', true).get()
  let matchRecordCount = 0
  matchRecordSnapshot.forEach(doc => {
    matchRecordCount += 1
  })
  res.json({
    matchRecordCount
  })
})
exports.adminPortal = functions.https.onRequest(app)
