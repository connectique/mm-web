module.exports = {
  /*
  ** Headers of the page
  */
  head: {
    title: 'Montessori Match',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Job site for Montessori educators and schools' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons' }
    ]
  },
  env: {
    FIREBASE_STORAGE_BUCKET: process.env.FIREBASE_STORAGE_BUCKET,
    FIREBASE_DATABASE_URL: process.env.FIREBASE_DATABASE_URL,
    FIREBASE_PROJECT_ID: process.env.FIREBASE_PROJECT_ID,
    FIREBASE_API_KEY: process.env.FIREBASE_API_KEY,
    PUBSUB_TOPIC_NAME: process.env.PUBSUB_TOPIC_NAME,
    SUMO_ENDPOINT: process.env.SUMO_ENDPOINT
  },
  plugins: [
    '~/plugins/vuetify.js',
    {
      src: '~/plugins/localStorage.js',
      ssr: false
    }
  ],
  router: {
    middleware: 'checkUser'
  },
  modules: [
    '@nuxtjs/axios',
    [
      '@nuxtjs/sentry'
    ],
    [
      '@nuxtjs/google-analytics',
      {
        id: 'UA-42341748-2'
      }
    ]
  ],
  sentry: {
    public_dsn: 'https://a0fee86558d6409283558407c3a96c19@sentry.io/270167'
  },
  axios: {},
  css: [
    '~/assets/style/app.styl'
  ],
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#3B8070' },
  /*
  ** Build configuration
  */
  build: {
    vendor: ['vuetify', 'axios', 'sumo-logger'],
    extractCSS: true,
    /*
    ** Run ESLINT on save
    */
    extend (config, ctx) {
      if (ctx.dev && process.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  }
}
