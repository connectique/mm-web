import * as axios from 'axios'

// process.env is interpolated from nuxt-config at build time
const topicName = process.env.PUBSUB_TOPIC_NAME
const matchAlgorithmUrl = `https://us-central1-connectiqueco.cloudfunctions.net/matchalgorithmrequest`

/**
 * Sends info to pub/sub for consumption by algorithm service.
 * Since pub/sub client requires 'fs' library and does not work in browser
 * we send to a cloud function via https that then passes it on to pub/sub.
 * @param {string} token
 * @param {string} matchingProfileID
 * @param {string} memberType
 * @returns {Promise}
 */
export function runAlgorithmForMatchingProfileId (userId, matchingProfileID, memberType) {
  // reject if missing params
  if (!(userId && matchingProfileID && memberType)) {
    return Promise.reject(new Error('missing required parameters'))
  }
  // make http call
  return axios.post(matchAlgorithmUrl, {
    topicName,
    message: {
      userId,
      matchingProfileID,
      memberType
    }
  })
    .catch(err => {
      console.log(err)
    })
}
