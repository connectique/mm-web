import Firestore from './firestore'

/**
 * Gets the memberType property of a user
 * @param {string} userId
 * @returns {Promise{string|Error}}
 */
export async function getMemberType (userId) {
  if (!userId) {
    return Promise.reject(new Error('missing required userId'))
  }

  const doc = await Firestore.collection('users').doc(userId).get()
  if (doc.exists) {
    return doc.data().memberType
  }
}

/**
 * Gets all private and public data for a user
 * @param {string} userId
 * @returns {Promise<PublicUserInfo|Error>}
 */
export async function getAllUserData (userId) {
  if (!userId) {
    return Promise.reject(new Error('missing required userId'))
  }

  const privateInfoDoc = await Firestore.collection('users').doc(userId).get()
  const privateInfo = privateInfoDoc.data()

  const publicInfoDoc = await Firestore.collection('publicUserInfo').doc(userId).get()
  const publicInfo = publicInfoDoc.data()

  // returns in composite object
  return {
    publicInfo,
    privateInfo
  }
}

/**
 * Sets (creates or overwrites) public and private user data
 * @param {string} userId
 * @param {PublicUserInfo} publicInfo
 * @param {SchoolPrivateInfo|EducatorPrivateInfo} privateInfo
 * @returns {Promise<undefined|Error>}
 */
export function setAllUserData (userId, publicInfo, privateInfo) {
  if (!(userId && publicInfo && privateInfo)) {
    return Promise.reject(new Error('missing required parameter'))
  }

  Firestore.collection('users').doc(userId).set(publicInfo)
  Firestore.collection('publicUserInfo').doc(userId).set(privateInfo)
}

/**
 * Updates (selective fields, so you can pass a partial object) public and private user data
 * @param {string} userId
 * @param {PublicUserInfo} publicInfo
 * @param {SchoolPrivateInfo|EducatorPrivateInfo} privateInfo
 * @returns {Promise<undefined|Error>}
 */
export function updateAllUserData (userId, publicInfo, privateInfo) {
  if (!(userId && publicInfo && privateInfo)) {
    return Promise.reject(new Error('missing required parameter'))
  }

  Firestore.collection('users').doc(userId).update(publicInfo)
  Firestore.collection('publicUserInfo').doc(userId).update(privateInfo)
}

export function setMemberType (userId, memberType) {
  if (!(userId && memberType)) {
    return Promise.reject(new Error('missing required parameter'))
  }

  return Firestore.collection('users').doc(userId).set({
    memberType
  })
}

/**
 * Gets PublicUserInfo object for a user
 * @param {string} userId
 * @returns {Promise<PublicUserInfo|Error>}
 */
export async function getPublicInfo (userId) {
  if (!userId) {
    return Promise.reject(new Error('missing required userId'))
  }

  const doc = await Firestore.collection('publicUserInfo').doc(userId).get()
  return doc.data()
}
