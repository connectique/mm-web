import Firestore from './firestore'

/**
 * Gets the id of a chat history between two users
 * @param {string} userIdOne
 * @param {string} userIdTwo
 * @returns {string}
 */
export async function getChatHistoryId (userIdOne, userIdTwo) {
  let snapshot
  try {
    snapshot = await Firestore.collection('chatRecords').where('userId', '==', userIdOne)
      .where('chatPartnerId', '==', userIdTwo)
      .get()
  } catch (err) {
    return new Error(err)
  }
  let docId
  snapshot.forEach(doc => {
    docId = doc.data().chatId
  })
  return docId
}

/**
 * Writes a message into a chat history, including a timestamp,
 * the user who wrote the message, and the text of the message
 * @param {string} chatHistoryId
 * @param {string} userId
 * @param {string} text
 */
export async function writeMessage (chatHistoryId, userId, text) {
  const timestamp = Date.now()
  try {
    await Firestore.collection('chatHistories').doc(chatHistoryId).collection('messages').add({
      userId,
      text,
      timestamp
    })
  } catch (err) {
    return new Error(err)
  }
}
