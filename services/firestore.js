import Firebase from './firebase'

const Firestore = Firebase.firestore()
const settings = {
  timestampsInSnapshots: true
}
Firestore.settings(settings)

export default Firestore
