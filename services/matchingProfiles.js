import Firestore from './firestore'
import { getPublicInfo } from './users'

/**
 * Creates a new matching profile for the user
 * @param {string} userId
 * @param {*} memberType
 * @returns {Promise<string|error>}
 */
export async function createMatchingProfile (userId, memberType) {
  // reject if missing parameters
  if (!(userId && memberType)) {
    return Promise.reject(new Error('missing required parameters'))
  }
  try {
    const docRef = await Firestore.collection(`matchingProfiles/`).add({
      name: 'My Matching Profile',
      userId: userId,
      memberType,
      role: 1,
      created: Date.now(),
      updated: Date.now(),
      complete: false,
      active: false,
      ageRanges: [],
      educationTypes: [],
      trainingTypes: [],
      organizationTypes: [],
      locationTypes: [],
      locationTypesWeight: 10,
      sizes: [],
      sizesWeight: 10,
      calendarTypes: [],
      calendarTypesWeight: 10,
      zipCodes: [],
      zipCodesWeight: 5
    })
    return docRef.id
  } catch (err) {
    return new Error(`Error creating matching profile: ${err}`)
  }
}

/**
 * Sets the matchingProfile's active state to false,
 * and sets valid prop on all matchRecords with that matchingProfileId
 * to false
 * @param {string} matchingProfileId
 */
export async function deactivateMatchingProfile (matchingProfileId) {
  const currentTime = Date.now()

  // deactivate the matching profile
  try {
    Firestore.collection('matchingProfiles').doc(matchingProfileId).update({
      active: false,
      updated: currentTime
    })
  } catch (err) {
    return new Error(err)
  }

  // deactivate all the matchRecords and profileMatchMaps
  let profileMatchMaps
  try {
    profileMatchMaps = await Firestore.collection('profileMatchMaps')
      .where('matchingProfileId', '==', matchingProfileId).get()
  } catch (err) {
    return new Error(err)
  }

  profileMatchMaps.forEach(async doc => {
    const data = doc.data()
    try {
      const matchRecord = await Firestore.collection('matchRecords').doc(data.matchRecordId).get().data()
      Firestore.collection('profileMatchMaps').doc(matchRecord.profileAMatchMapId).update({
        valid: false,
        updated: currentTime
      })
      Firestore.collection('profileMatchMaps').doc(matchRecord.profileBMatchMapId).update({
        valid: false,
        updated: currentTime
      })
      Firestore.collection('matchRecords').doc(data.matchRecordId).update({
        valid: false,
        updated: currentTime
      })
    } catch (err) {
      return new Error(err)
    }
  })
}

/**
 * Deletes the matchingProfile and all associated matchRecords
 * @param {string} matchingProfileId
 */
export async function deleteMatchingProfile (matchingProfileId) {
  Firestore.collection('matchingProfiles').doc(matchingProfileId).delete()

  // delete all the matchRecords and profileMatchMaps
  const profileMatchMaps = await Firestore.collection('profileMatchMaps')
    .where('matchingProfileId', '==', matchingProfileId).get()

  profileMatchMaps.forEach(async doc => {
    const data = doc.data()
    const matchRecord = await Firestore.collection('matchRecords').doc(data.matchRecordId).get().data()
    Firestore.collection('profileMatchMaps').doc(matchRecord.profileAMatchMapId).delete()
    Firestore.collection('profileMatchMaps').doc(matchRecord.profileBMatchMapId).delete()
    doc.delete()
  })
}

/**
 * Updates fields in a matchingProfile
 * Data param is an object with keys that are the fields you want to update.
 * @param {string} matchingProfileId
 * @param {Object} data
 * @returns {Promise<>}
 */
export function updateMatchingProfile (matchingProfileId, data) {
  if (!(matchingProfileId && data)) {
    return Promise.reject(new Error('missing required parameters'))
  }
  data.updated = Date.now()
  return Firestore.collection('matchingProfiles/')
    .doc(matchingProfileId)
    .update(data)
}

/**
 * Gets all the matching profiles for a given user
 * @param {string} userId
 * @returns {Promise<MatchingProfile[]>}
 */
export function getAllMatchingProfiles (userId) {
  if (!userId) {
    return Promise.reject(new Error('missing userId'))
  }
  return Firestore.collection(`matchingProfiles/`).where('userId', '==', userId)
    .get()
}

/**
 * Gets a single matching profile
 * @param {string} profileId
 * @returns {Promise<MatchingProfile|null>}
 */
export async function getMatchingProfileById (profileId) {
  if (!profileId) {
    return Promise.reject(new Error('missing profileId'))
  }
  const doc = await Firestore.collection(`matchingProfiles`).doc(profileId).get()
  if (doc.exists) {
    return doc.data()
  }
  return null
}

/**
 * Gets a list of confirmed connections for a matching profile.
 * A confirmed connection is select data from a MatchRecord
 * where both users have shown interest.
 * @param {string} matchingProfileId
 * @returns {Promise<ConfirmedConnection[]>}
 */
export async function getAllConfirmedMatchRecords (matchingProfileId) {
  if (!matchingProfileId) {
    return Promise.reject(new Error('missing matchingProfileId'))
  }

  // create array to collection docs
  const confirmedConnections = []

  // get all valid profileMatchMaps
  const matchMaps = await Firestore.collection('profileMatchMaps')
    .where('matchingProfileId', '==', matchingProfileId)
    .where('valid', '==', true)
    .get()

  await matchMaps.forEach(async matchMap => {
    const mmData = matchMap.data()

    // are we user a or b?
    let label
    let otherLabel
    if (mmData.recordUserLabel === 'A') {
      label = 'A'
      otherLabel = 'B'
    } else {
      label = 'B'
      otherLabel = 'A'
    }

    // get match records that are valid, where both users have confirmed
    const matchRecords = await Firestore.collection('matchRecords')
      .where(`matchingProfileId${label}`, '==', matchingProfileId)
      .where('confirmedByUserA', '==', true)
      .where('confirmedByUserB', '==', true)
      .where('deniedByUserA', '==', false)
      .where('deniedByUserB', '==', false)
      .where('valid', '==', true)
      .get()
    // loop through and retrieve only needed data for display from object
    await matchRecords.forEach(record => {
      const confirmedConnection = {}
      const rData = record.data()
      confirmedConnection.percent = rData.percent
      confirmedConnection.matchRecordId = rData.id
      confirmedConnection.otherMatchingProfileId = rData.otherMatchingProfileId
      // get public user info about each connection and add to object
      // and push object into array
      return getPublicInfo(rData[`userId${otherLabel}`])
        .then(info => {
          confirmedConnection.displayName = info.displayName
          confirmedConnection.imageUrl = info.avatarUrl
          confirmedConnections.push(confirmedConnection)
        })
    })
  })
  return confirmedConnections
}

/**
 * Gets the info required for displaying details of a confirmed connection
 * @param {string} matchRecordId
 * @returns {Promise<ConfirmedConnectionData|Error>}
 */
export async function getSingleConfirmedMatchRecord (matchRecordId, userId) {
  if (!matchRecordId) {
    return Promise.reject(new Error('missing required matchRecordId'))
  }

  // set up vars to put docs into
  let matchRecord
  let matchingProfile
  let otherMatchingProfile
  let otherUserProfile

  // try async retrievals, reject if they fail
  try {
    const doc = await Firestore.collection('matchRecords/').doc(matchRecordId).get()
    matchRecord = doc.data()
  } catch (e) {
    return Promise.reject(new Error(`error getting match record: ${e}`))
  }

  // are we user a or b?
  let label
  let otherLabel
  if (userId === matchRecord.userIdA) {
    label = 'A'
    otherLabel = 'B'
  } else {
    label = 'B'
    otherLabel = 'A'
  }

  try {
    const doc = await Firestore.collection('matchingProfiles/').doc(matchRecord[`matchingProfileId${label}`]).get()
    matchingProfile = doc.data()
  } catch (e) {
    return Promise.reject(new Error(`error getting matching profile: ${e}`))
  }
  try {
    const doc = await Firestore.collection('matchingProfiles/').doc(matchRecord[`matchingProfileId${otherLabel}`]).get()
    otherMatchingProfile = doc.data()
  } catch (e) {
    return Promise.reject(new Error(`error getting other matching profile: ${e}`))
  }
  try {
    otherUserProfile = await getPublicInfo(matchRecord[`userId${otherLabel}`])
  } catch (e) {
    return Promise.reject(new Error(`error getting other user profile: ${e}`))
  }

  // return composite object
  return {
    matchRecord,
    matchingProfile,
    otherMatchingProfile,
    otherUserProfile,
    otherUserId: matchRecord[`userId${otherLabel}`]
  }
}

/**
 * Gets all match records that are potential matches.
 * This returns some records that could be confirmed connections,
 * so they need to be filtered out.
 * @param {string} matchingProfileId
 * @returns {Promise<PotentialMatch|Error>}
 */
export async function getAllPotentialMatchRecords (matchingProfileId) {
  // set up array to be returned
  const potentialMatches = []

  // get all valid profileMatchMaps
  const matchMaps = await Firestore.collection('profileMatchMaps')
    .where('matchingProfileId', '==', matchingProfileId)
    .where('valid', '==', true)
    .get()

  await matchMaps.forEach(async matchMap => {
    const mmData = matchMap.data()

    // are we user a or b?
    let label
    let otherLabel
    if (mmData.recordUserLabel === 'A') {
      label = 'A'
      otherLabel = 'B'
    } else {
      label = 'B'
      otherLabel = 'A'
    }
    const matchRecords = await Firestore.collection('matchRecords/')
      .where(`matchingProfileId${label}`, '==', matchingProfileId)
      .where('deniedByUserA', '==', false)
      .where('deniedByUserB', '==', false)
      .where('valid', '==', true)
      .get()

    await matchRecords.forEach(record => {
      const potentialMatch = {}
      const rData = record.data()
      // filter out records that are confirmed connections
      if (!(rData.confirmedByUserA && rData.confirmedByUserB)) {
        potentialMatch.percent = rData.percent
        potentialMatch.matchRecordId = rData.id
        potentialMatch.otherMatchingProfileId = rData.otherMatchingProfileId
        return getPublicInfo(rData[`userId${otherLabel}`])
          .then(info => {
            potentialMatch.displayName = info.displayName
            potentialMatches.push(potentialMatch)
          })
      }
    })
  })
  return potentialMatches
}

/**
 * Gets the info required for displaying details of a potential match
 * @param {string} matchRecordId
 * @returns {Promise<PotentialMatchData|Error>}
 */
export async function getSinglePotentialMatchRecord (matchRecordId, userId) {
  if (!matchRecordId) {
    return Promise.reject(new Error('missing required matchRecordId'))
  }

  // set up vars to put docs into
  let matchRecord
  let matchingProfile
  let otherMatchingProfile
  let otherUserProfile

  // try async retrievals, reject if they fail
  try {
    const doc = await Firestore.collection('matchRecords/').doc(matchRecordId).get()
    matchRecord = doc.data()
  } catch (e) {
    return Promise.reject(new Error(`error getting match record: ${e}`))
  }

  // are we user a or b?
  let label
  let otherLabel
  if (userId === matchRecord.userIdA) {
    label = 'A'
    otherLabel = 'B'
  } else {
    label = 'B'
    otherLabel = 'A'
  }

  try {
    const doc = await Firestore.collection('matchingProfiles/').doc(matchRecord[`matchingProfileId${label}`]).get()
    matchingProfile = doc.data()
  } catch (e) {
    return Promise.reject(new Error(`error getting matching profile: ${e}`))
  }
  try {
    const doc = await Firestore.collection('matchingProfiles/').doc(matchRecord[`matchingProfileId${otherLabel}`]).get()
    otherMatchingProfile = doc.data()
  } catch (e) {
    return Promise.reject(new Error(`error getting other matching profile: ${e}`))
  }
  try {
    otherUserProfile = await getPublicInfo(matchRecord[`userId${otherLabel}`])
  } catch (e) {
    return Promise.reject(new Error(`error getting other user profile: ${e}`))
  }

  // return composite object
  return {
    matchRecord,
    matchingProfile,
    otherMatchingProfile,
    otherUserProfile
  }
}

/**
 * Changes a matchRecord to show that the current user has shown interest
 * @param {string} matchRecordId
 * @returns {Promise<MatchRecord|Error>}
 */
export async function showInterestInMatchRecord (matchRecordId, userId) {
  if (!matchRecordId) {
    return Promise.reject(new Error('missing required matchRecordId'))
  }

  // get current time for updating 'updated' field
  const time = Date.now()
  const recordRef = Firestore.collection('matchRecords/').doc(matchRecordId)
  const record = await Firestore.collection('matchRecords/').doc(matchRecordId).get()
  const recordData = record.data()

  // are we user a or b?
  let label
  if (userId === recordData.userIdA) {
    label = 'A'
  } else {
    label = 'B'
  }

  // update fields
  recordData[`confirmedByUser${label}`] = true
  recordData.updated = time
  recordRef.update(recordData)

  // return this user's new matchRecord
  return recordData
}

/**
 * Updates a MatchRecord and its pair to show current user has removed interest
 * @param {string} matchRecordId
 * @returns {Promise<MatchRecord|Error>}
 */
export async function removeInterestInMatchRecord (matchRecordId, userId) {
  if (!matchRecordId) {
    return Promise.reject(new Error('missing required matchRecordId'))
  }

  const time = Date.now()
  const record = await Firestore.collection('matchRecords/').doc(matchRecordId)
  let recordDoc = await record.get()
  const recordData = recordDoc.data()

  // are we user a or b?
  let label
  if (userId === recordData.userIdA) {
    label = 'A'
  } else {
    label = 'B'
  }

  recordData[`confirmedByUser${label}`] = false
  recordData.updated = time

  record.update(recordData)

  return recordData
}

/**
 * Updates a record and its pair to show the current user has denied it
 * @param {string} matchRecordId
 */
export async function denyMatchRecord (matchRecordId, userId) {
  if (!matchRecordId) {
    return Promise.reject(new Error('missing required matchRecordId'))
  }

  const time = Date.now()
  const recordRef = await Firestore.collection('matchRecords/').doc(matchRecordId)
  const record = await Firestore.collection('matchRecords/').doc(matchRecordId).get()

  // are we user a or b?
  let updateData
  if (userId === record.data().userIdA) {
    updateData = {
      deniedByUserA: true,
      valid: false,
      updated: time
    }
  } else {
    updateData = {
      deniedByUserB: true,
      valid: false,
      updated: time
    }
  }

  recordRef.update(updateData)
}
