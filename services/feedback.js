import Firestore from './firestore'

/**
 * Writes a FeedbackItem into the database
 * @param {string} text
 * @param {string} email
 * @param {string} currentPage
 */
export function writeFeedback (text, email, currentPage) {
  const timestamp = Date.now()
  try {
    Firestore.collection('feedback').add({
      text,
      email,
      currentPage,
      timestamp
    })
  } catch (err) {
    return new Error(err)
  }
}

/**
 * A feedback item object
 * @typedef {Object} FeedbackItem
 * @property {string} text
 * @property {string} email
 * @property {string} currentPage
 * @property {number} timestamp - unix date
 */

/**
 * Gets all feedback objects from the database
 * @returns {FeedbackItem[]}
 */
export async function getAllFeedback () {
  const feedbackItemsSnapshot = await Firestore.collection('feedback').orderBy('timestamp').get()
  const items = []
  await feedbackItemsSnapshot.forEach(doc => {
    items.push({
      id: doc.id,
      data: doc.data()
    })
  })
  return items
}

/**
 * Deletes a feedback item from the database
 * @param {string} docId
 */
export function deleteFeedbackItem (docId) {
  return Firestore.collection('feedback').doc(docId).delete()
}
