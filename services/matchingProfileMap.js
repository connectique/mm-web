// maps db values to display values
export default {
  role: {
    1: 'Teacher',
    2: 'Assistant'
  },
  ageRanges: {
    1: '0 to 3',
    2: '3 to 6',
    3: '6 to 9',
    4: '9 to 12',
    5: '12+'
  },
  educationTypes: {
    1: 'High School',
    2: 'Associate',
    3: 'Bachelor',
    4: 'Master',
    5: 'Doctorate'
  },
  trainingTypes: {
    1: 'Association Montessori Internationale',
    2: 'American Montessori Society',
    3: 'Center for Guided Studies',
    4: 'Pan-American Montessori Society',
    5: 'Other MACTE Accredited Program',
    6: 'Other Non-MACTE Accredited Program'
  },
  organizationTypes: {
    1: 'Public District',
    2: 'Public Charter',
    3: 'Public Magnet',
    4: 'Private Not For Profit',
    5: 'Private For Profit'
  },
  locationTypes: {
    1: 'Rural',
    2: 'Suburban',
    3: 'Small City',
    4: 'Urban'
  },
  sizes: {
    1: '4 or fewer',
    2: '5 to 9',
    3: '10 to 19',
    4: '20+'
  },
  calendarTypes: {
    1: 'Traditional',
    2: 'Year Round'
  },
  traits: {
    educatorTraits: {
      1: 'I like to collaborate with my fellow lead teachers and align our work in the classroom.',
      2: 'I prefer to work independently and focus on my classroom specifically, without worrying what others are doing.',
      3: 'My classroom uses exclusively cursive.',
      4: 'My classroom uses exclusively print.',
      5: 'My classroom uses a mix of cursive and print.',
      6: 'I like to adhere to my albums and training very closely and I don’t really stray from them.',
      7: 'I like to explore extensions and new materials beyond what is in my albums or what I learned in my training.',
      8: 'I enjoy inviting parents into the classroom to volunteer for special projects like reading books or baking.',
      9: 'I prefer to keep adults out of the classroom during the work cycle.',
      10: 'My assistant teachers get a lot of space to take ownership of certain projects or areas of the classroom.',
      11: 'My assistant teachers are given detailed instructions on what to do in the classroom and I expect them to stick to it closely so we can keep things consistent.',
      12: 'I like to incorporate technology appropriately into the classroom.',
      13: 'I prefer to keep technology out of the classroom.',
      14: 'I love to do culinary activities with the children.',
      15: 'I love to do art/maker activities with the children.',
      16: 'I love to do science activities with the children.',
      17: 'I love to do movement/games activities with the children.',
      18: 'I love to do music activities with the children.',
      19: 'I love to do gardening and outdoor exploration activities with the children',
      20: 'I tend to be very tidy and organized. I like to keep the classroom orderly.',
      21: 'I like to get to know my colleagues and form strong personal connections with them.',
      22: 'I love to play actively on the playground with the children.',
      23: 'I love to sit and observe the classroom and keep detailed records on each child.',
      24: 'I like to collaborate on projects and events outside of my classroom that involve the whole school.',
      25: 'I have a specific interest in using inclusive, social justice oriented approaches in the classroom (universal design, anti-bias, anti-racist, etc).',
      26: 'I resond calmly and quickly during emergencies.',
      27: 'I\'m skilled at making myself a nearly invisible adult in the classroom during the work cycle.'
    },
    schoolTraits: {
      1: 'They like to collaborate with their fellow lead teachers and align their work in the classroom.',
      2: 'They prefer to work independently and focus on their classroom specifically, without worrying what others are doing.',
      3: 'Their classroom uses exclusively cursive.',
      4: 'Their classroom uses exclusively print.',
      5: 'Their classroom uses a mix of cursive and print.',
      6: 'They like to adhere to their albums and training very closely and they don’t really stray from them.',
      7: 'They like to explore extensions and new materials beyond what is in their albums or what they learned in my training.',
      8: 'They enjoy inviting parents into the classroom to volunteer for special projects like reading books or baking.',
      9: 'They prefer to keep adults out of the classroom during the work cycle.',
      10: 'Their assistant teachers get a lot of space to take ownership of certain projects or areas of the classroom.',
      11: 'Their assistant teachers are given detailed instructions on what to do in the classroom and they expect them to stick to it closely so they can keep things consistent.',
      12: 'They like to incorporate technology appropriately into the classroom.',
      13: 'They prefer to keep technology out of the classroom.',
      14: 'They love to do culinary activities with the children.',
      15: 'They love to do art/maker activities with the children.',
      16: 'They love to do science activities with the children.',
      17: 'They love to do movement/games activities with the children.',
      18: 'They love to do music activities with the children.',
      19: 'They love to do gardening and outdoor exploration activities with the children',
      20: 'They tend to be very tidy and organized. I like to keep the classroom orderly.',
      21: 'They like to get to know my colleagues and form strong personal connections with them.',
      22: 'They love to play actively on the playground with the children.',
      23: 'They love to sit and observe the classroom and keep detailed records on each child.',
      24: 'They like to collaborate on projects and events outside of my classroom that involve the whole school.',
      25: 'They have a specific interest in using inclusive, social justice oriented approaches in the classroom (universal design, anti-bias, anti-racist, etc).',
      26: 'They respond calmly and quickly during emergencies.',
      27: 'They are skilled at making themsleves a nearly invisible adult in the classroom during the work cycle.'
    }
  }
}
