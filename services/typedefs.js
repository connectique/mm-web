/**
 * @typedef {Object} ZipCode
 * @property {number} code
 * @property {number} distance
 */

/**
 * @typedef {Object} MatcingProfile
 * @property {string} id
 * @property {string} name
 * @property {string} userId
 * @property {number} role
 * @property {number[]} ageRanges
 * @property {number[]} educationTypes
 * @property {number[]} organizationTypes
 * @property {number[]} calendarTypes
 * @property {number} calendarTypesWeight
 * @property {number[]} locationTypes
 * @property {number} locationTypesWeight
 * @property {number[]} sizes
 * @property {number} sizesWeight
 * @property {number[]} traits
 * @property {number} traitsWeight
 * @property {ZipCode[]} zipCodes
 * @property {number} zipCodesWeight
 * @property {number} created
 * @property {number} updated
 * @property {boolean} complete
 */

/**
 * @typedef {Object} MatchRecord
 * @property {string} id
 * @property {string} userId
 * @property {string} matchingProfileId
 * @property {string} otherMatchingProfileId
 * @property {string} otherUserId
 * @property {number} percent
 * @property {boolean} confirmedByUser
 * @property {boolean} confirmedByOther
 * @property {boolean} deniedByUser
 * @property {boolean} deniedByOther
 * @property {boolean} valid
 * @property {number} created
 * @property {number} updated
 */

/**
 * @typedef {Object} ConfirmedConnection
 * @property {number} percent
 * @property {string} matchRecordId
 * @property {string} otherMatchingProfileId
 * @property {string} displayName
 * @property {string} imageUrl
 */

/**
 * @typedef {Object} PotentialMatch
 * @property {number} percent
 * @property {string} matchRecordId
 * @property {string} otherMatchingProfileId
 * @property {string} displayName
 */

/**
 * @typedef {Object} UserProfile
 * @property {string} displayName
 * @property {string} imageUrl
 */

/**
 * @typedef {Object} ConfirmedConnectionData
 * @property {MatchRecord} matchRecord
 * @property {MatchingProfile} matchingProfile
 * @property {MatchingProfile} otherMatchingProfile
 * @property {UserProfile} userProfile
 * @property {string} otherUserId
 */

/**
 * @typedef {Object} PotentialMatchData
 * @property {MatchRecord} matchRecord
 * @property {MatchingProfile} matchingProfile
 * @property {MatchingProfile} otherMatchingProfile
 * @property {UserProfile} userProfile
 */

/**
 * @typedef {Object} SchoolPrivateInfo
 * @property {string} name
 * @property {string} memberType
 */

/**
 * @typedef {Object} EducatorPrivateInfo
 * @property {string} name
 * @property {string} memberType
 */

/**
 * @typedef {Object} PublicUserInfo
 * @property {string} displayName
 * @property {string} description
 */

/**
 * @typedef {Object} AllUserInfo
 * @property {SchoolPrivateInfo|EducatorPrivateInfo} privateInfo
 * @property {PublicUserInfo} publicInfo
 */
