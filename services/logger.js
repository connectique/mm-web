import * as SumoLogger from 'sumo-logger'

const opts = {
  endpoint: process.env.SUMO_ENDPOINT,
  sendErrors: true,
  interval: 20000,
  hostName: 'https://www.montessorimatch.com'
}

export const logger = new SumoLogger(opts)

export function logError (message, userId, page) {
  logger.log({
    timestamp: new Date(),
    message,
    userId,
    page,
    level: 'error'
  })
}

export function logInfo (message, userId, page) {
  logger.log({
    timestamp: new Date(),
    message,
    userId,
    page,
    level: 'info'
  })
}
