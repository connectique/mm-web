import * as firebase from 'firebase'
require('firebase/firestore')

var config = {
  apiKey: process.env.FIREBASE_API_KEY,
  databaseURL: process.env.FIREBASE_DATABASE_URL,
  projectId: process.env.FIREBASE_PROJECT_ID,
  storageBucket: process.env.FIREBASE_STORAGE_BUCKET
}

// ensure that Firebase is only initialized once
const Firebase = !firebase.apps.length ? firebase.initializeApp(config) : firebase.app()

// Firebase should be imported from here anywhere else it is used so there is only one instance
export default Firebase
