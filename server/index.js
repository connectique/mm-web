import Koa from 'koa'
import { Nuxt, Builder } from 'nuxt'
import * as Router from 'koa-router'

if (process.env.NODE_ENV === 'production') {
  require('@google-cloud/debug-agent').start({ allowExpressions: true })
}

async function start () {
  const app = new Koa()
  if (process.env.NODE_ENV === 'production') {
    app.env = 'production'
  }
  const router = new Router()
  const host = process.env.HOST || '127.0.0.1'
  const port = process.env.PORT || 8080

  // Import and Set Nuxt.js options
  let config = require('../nuxt.config.js')
  config.dev = !(app.env === 'production')

  // Instantiate nuxt.js
  const nuxt = new Nuxt(config)

  router.get('/_ah/health', (ctx, next) => {
    ctx.status = 200
  })

  app.use(router.routes())

  // Build in development
  if (config.dev) {
    const builder = new Builder(nuxt)
    await builder.build()
  }

  app.use(async (ctx, next) => {
    await next()
    ctx.status = 200 // koa defaults to 404 when it sees that status is unset
    return new Promise((resolve, reject) => {
      ctx.res.on('close', resolve)
      ctx.res.on('finish', resolve)
      nuxt.render(ctx.req, ctx.res, promise => {
        // nuxt.render passes a rejected promise into callback on error.
        promise.then(resolve).catch(reject)
      })
    })
  })

  app.listen(port)
  console.log('Server listening on ' + host + ':' + port) // eslint-disable-line no-console
}

start()
