import Firebase from '../services/firebase'
import { getMemberType } from '../services/users'

/**
 * If there is a current user, make sure that store
 * is set with correct data. This avoids situations in
 * which a user is logged in (Firebase keeps tokens in localstorage)
 * but the state store does not reflect that.
 */
export default async function ({ store }) {
  if (store.state.loggedIn === false) {
    const user = Firebase.auth().currentUser
    if (user) {
      store.commit('logIn')
      store.commit('setUser', user)
      if (!store.state.memberType) {
        const memberType = await getMemberType(user.uid)
        store.commit('setMemberType', memberType)
      }
    }
  }
}
