import Firebase from '../services/firebase'
import { getMemberType } from '../services/users'

/**
 * Checks if a user has the memberType 'admin'
 * If not, or if there is no logged in user,
 * redirects to index
 */
export default async function ({ redirect }) {
  const user = Firebase.auth().currentUser
  if (user) {
    try {
      const memberType = await getMemberType(user.uid)
      if (memberType !== 'admin') {
        return redirect('/')
      }
    } catch (err) {
      console.error(err)
      return redirect('/')
    }
  } else {
    return redirect('/')
  }
}
