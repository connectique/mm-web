import Firebase from '../services/firebase'

/**
 * Checks if there is a currently logged in user
 */
export default function ({ store, redirect }) {
  let user = store.state.user || Firebase.auth().currentUser
  if (!user || store.state.loggedIn === false) {
    return redirect('/auth/login')
  }
}
