import Firebase from '../services/firebase'
import { getAllUserData } from '../services/users'

/**
 * Checks if the user has a completely filled out profile.
 * If they don't, redirect them to profile update page.
 */
export default async function ({ redirect }) {
  const user = Firebase.auth().currentUser
  if (user) {
    try {
      const { privateInfo, publicInfo } = await getAllUserData(user.uid)
      if (privateInfo.memberType !== 'school' && privateInfo.memberType !== 'educator' && privateInfo.memberType !== 'admin') {
        return redirect('/my-profile/member-type')
      }
      if (privateInfo.memberType !== 'admin') {
        if (!privateInfo.name || !publicInfo.displayName || !publicInfo.description) {
          return redirect('/my-profile/finish')
        }
      }
    } catch (err) {
      console.error(err)
      return redirect('/')
    }
  } else {
    return redirect('/')
  }
}
