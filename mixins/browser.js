const { detect } = require('detect-browser')

export const browserMixin = {
  methods: {
    detectSafari10 () {
      const browser = detect()
      if (browser.name === 'safari' && browser.version.includes('10.')) {
        return true
      }
      return false
    }
  }
}
